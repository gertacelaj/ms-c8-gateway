package it.cgmconsulting.msc8.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MsC8GatewayApplication {

	public static void main(String[] args) {

		SpringApplication.run(MsC8GatewayApplication.class, args);
	}

}
